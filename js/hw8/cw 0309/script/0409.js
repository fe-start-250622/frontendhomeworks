import createTag from "./createTag.js"
/*
window.onload = () => {
    // 1
    const divs = document.querySelectorAll("div");
    //2
    const [...divs2] = document.getElementsByTagName("div");
    //3
    const divs3 = document.getElementsByClassName("element");

    filter(divs2);
}

function filter(arr) {
    arr.forEach((div) => {
       div.onclick = function () {
           div.style.color = "red";
           div.style.border = "1px solid blue";
       }
    });
}
*/ 

/*
Згенерувати теги теги через javascript. Додати на сторінку семантичні теги та метагеги опису сторінки.
 прописати стилі для для елементів використовуючи css id та класи
https://ru.megaindex.com/blog/files/images/semantic-markup-example.jpg

При подвійному кліку на тег має з'явитись input який прийматиме текст для тега по якому натиснули та 
новий фоновий колір
*/


window.onload = () => {
let body = document.body;
const topHeader = createTag("header", "top-header", null, "header"),
main = createTag("main"),

sectionLeft = createTag("section", "section-l", null, "section"),
sectionRight = createTag("section", "section-r", null, "section"),
articleOne = createTag("article", "article-1", null, "article"),
articleTwo = createTag("article", "article-2", null, "article"),
footer = createTag("footer", "bottom-footer", null, "footer");
body.className = "body";
body.textContent = "body";



body.append(topHeader)
body.append(main)

topHeader.append(createTag("nav", "header-nav", null, "nav"));
main.append(sectionLeft, sectionRight);
sectionLeft.append(
    createTag("header","section-header", null, "header"),
    articleOne,
    articleTwo,
    createTag("footer", "footer-left", null, "footer")
)

sectionRight.append(
    createTag("header","section-header", null, "header"),
    createTag("nav", "nav-right", null, "nav")
)
articleOne.append(
    createTag("header","article-header", null, "header"),
    createTag("p", "article-p", null, "p"),
    createTag("p", "p-left", null, "p"),
    createTag("aside", "aside-right", null, "aside"),
    createTag("footer", "article-footer", null, "footer")
)
articleTwo.append(
    createTag("header","article-header", null, "header "),
    createTag("p", "article-p", null, "p"),
    createTag("p", "article-p", null, "p"),
    createTag("footer", "article-footer", null, "footer")
),
body.append(footer)
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

let tags = document.querySelectorAll("*");


window.addEventListener("load", () => {
    [...tags].forEach(item => item.addEventListener("dblclick", (e) => {
        e.target.style.backgroundColor = getRandomColor();
        e.target.textContent = prompt("введіть текст");
    }))
})
