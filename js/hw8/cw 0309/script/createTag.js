function createTag (tagName = "", classUserName = "", idName = "", content = ""){
    if(tagName === null || tagName === ""){
        console.error("Назва тегу відсутня");
        return ""
    }
    const newTag = document.createElement(tagName);

    if(classUserName !== "" && classUserName !== null){
        newTag.className = classUserName;
    }
    if(idName !== "" && idName !== null){
        newTag.id = idName;
    }
    if(content !== "" && content !== null){
        newTag.textContent = content;
    }
   /* newTag.ondblclick = function() {
    
        prompt("введіть текст");
        
  
    }*/
    
    return newTag
}

export default createTag;

//https://gitlab.com/Philip7e/22.09-fe-pro/-/blob/main/08_Lesson%208/Presentation/paterns.html