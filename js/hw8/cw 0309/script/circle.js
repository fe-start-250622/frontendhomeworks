import createTag from "./createTag.js"


/*Нарисовать на странице круг используя параметры, которые введет пользователь.
              
При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". 
Данная кнопка должна являться единственным контентом в теле HTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга.
 При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. 
 При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
*/


const createNewTextInput = () => {
    let body = document.body;
    const item = document.createElement("input");
    item.innerHTML = "NEW ITEM";
    body.append(item);
    item.setAttribute("type", "text");
    item.setAttribute("value", "");
    item.setAttribute("placeholder", "Введите диаметр круга, например: 100px");
    item.setAttribute("id", "size");
    const inputCircle = document.querySelector("#size");
    inputCircle.style.width = "400px";
}


const createNewButton = () => {
    let body = document.body;
    const item = document.createElement("input");
    item.innerHTML = "NEW ITEM";
    body.append(item);
    item.setAttribute("type", "button");
    item.setAttribute("value", "Нарисовать");
    item.setAttribute("id", "draw");

}


window.addEventListener("load", () => {
    document.querySelector("#circleDraw").onclick = function () {
        createNewTextInput()
        createNewButton ()
    }
    })
    

const createCircle = () => {
    
    let body = document.body;
    var number = 100;
    for (let i=0; i<=number; i++) {
    const divCircle = createTag("div", "circle", null, "circle");
    body.append(divCircle)
    
    }
    const inputCircle = document.querySelector("#size");
    const [...divss] = document.getElementsByTagName("div");
    divss.forEach((el)=>{  
        el.style.backgroundColor = `hsl(${Math.floor(Math.random()*360)},50%,50%)`;
        el.style.display = "inline-block";
        el.style.padding = parseFloat(inputCircle.value) / 2.0;
       
       })
   
}


window.addEventListener("click", () => {
    document.querySelector("#draw").onclick = function () {
        createCircle()

    }
    })


window.addEventListener("click", () => {
   
        const [...divss] = document.getElementsByTagName("div");
        [...divss].forEach(item => item.addEventListener("click", (e) => {
            if (item != null) {
                item.remove();
            }
        }))
    
    })


    