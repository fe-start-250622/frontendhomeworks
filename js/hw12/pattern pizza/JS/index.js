window.addEventListener('DOMContentLoaded', (event) => {

const radioButtons = document.querySelectorAll('input[name="size"]'),
ingredients = document.querySelectorAll(".ingridients > div"),
priceResult = document.querySelector(".price > p"),
saucesResult = document.querySelector(".sauces > p"),
topingResult = document.querySelector(".topings > p"),
imageDropArea = document.querySelector(".table");





let ingrPrice = "";
let totalPrice = 0;
let startPrice = 0;




for(let i = 0; i < radioButtons.length; i++){ 
    let radioButton =   radioButtons[i];
    radioButton.addEventListener('change', function(e) {
        let val = e.target.value;
        
        e.target.setAttribute('checked', 'true');
        
     for (let k = 0; k < radioButtons.length; k++) {
        if(k!=i) {
            radioButtons[k].removeAttribute('checked', 'true');
        }
    }

      if (val=== "small") {
        startPrice = 200;
        
        
    } 

     if (val=== "mid") {
        startPrice = 300;
       
        
    }
     if (val === "big") {
        startPrice = 400;
       
       
    }
    
      console.log(val);

      
      let priceArray;
      priceArray = ingrPrice.split(',');
      console.log(priceArray );

      totalPrice = sumPrice( priceArray, startPrice );
      priceResult.textContent = "ЦІНА:" + totalPrice;
      
      //console.log(startPrice);
    });
  }

  //console.log(startPrice);

    
   

function Ingredien(id, name, price ) { 
    
    this.id = id;
    this.ingredName = name;
    this.ingredPrice = price;


}


const ingreds = [
new Ingredien("moc1","Сир звичайний","10"), 
new Ingredien("moc2","Сир фета","20"), 
new Ingredien("moc3","Моцарелла",30), 
new Ingredien("telya","Телятина",30),
new Ingredien("vetch1","Помідори",10),
new Ingredien("vetch2","Гриби",20),
new Ingredien("sauceClassic","Кетчуп", 10),
new Ingredien("sauceBBQ","BBQ",20),
new Ingredien("sauceRikotta","Рікотта",30)
];

let map = new Map();

ingreds.forEach(function(elem) {

map.set(elem.id, elem.ingredName)
});

let map1 = new Map();

ingreds.forEach(function(elem) {

    map1.set(elem.id, elem.ingredPrice)
});




//В начале перетаскивания будем добавлять класс selected элементу списка, на котором было вызвано событие. 
//После окончания перетаскивания будем удалять этот класс.


ingredients.forEach(function(elem) {
    elem.addEventListener('dragstart', (e) => {
  e.target.classList.add('selected');
  console.log('dragStart')
})
});

ingredients.forEach(function(elem) {
    elem.addEventListener('dragend', (e) => {
  e.target.classList.remove('selected');
  console.log('dragEnd')
})
});



imageDropArea.addEventListener('dragover', (e) => {
    // Разрешаем сбрасывать элементы в эту область
    e.preventDefault();
    
    // Находим перемещаемый элемент
    const activeElement = document.querySelector('.selected');
    //console.log( activeElement);
    // Находим элемент, над которым в данный момент находится курсор
    const currentElement = e.target;
    // Проверяем, что событие сработало:
    // 1. не на том элементе, который мы перемещаем,
    // 2. именно на корже
   const isMoveable = activeElement !== currentElement && currentElement.classList.contains('.table');
   //console.log('dragOver')
    console.log(isMoveable );
    // Если нет, прерываем выполнение функции
    if (!isMoveable) {
      return;
    }
    
})





imageDropArea.addEventListener('drop', (e) => {
   
    e.preventDefault();
    const activeElement = document.querySelector('.selected');
    let droppedImg = imageDropArea.appendChild(activeElement.cloneNode());
    
    
    droppedImg.classList.replace("selected", "dropped");
    
    const sauceIdArr = ["sauceClassic","sauceBBQ","sauceRikotta"];
    if (sauceIdArr.includes(droppedImg.id)){
    saucesResult.textContent += map.get(droppedImg.id) + ",";
    } else {
        topingResult.textContent += map.get(droppedImg.id) + ",";
    }
    
   
    ingrPrice += map1.get(droppedImg.id)+",";

    let priceArray;
    priceArray = ingrPrice.split(',');
    console.log(priceArray );
   

    totalPrice = sumPrice( priceArray, startPrice );
    priceResult.textContent = "ЦІНА:" + totalPrice;


    console.log('Drop')
    
 });


 function sumPrice(arr, pr){
    let x = 0;

    for( let i = 0; i < arr.length; i++ ){
      x += +arr[i]; 
      
    }
    pr+=x;
    return pr;
    
  }




  const submitButton = document.getElementsByName("btnSubmit")[0],
  resetButton = document.getElementsByName("cancel")[0];




resetButton.addEventListener('click', (e) => {


  document.forms[1].reset(); // Reset the form

});

submitButton.addEventListener('click', (e) => {


  document.forms[1].requestSubmit(); // Submit the form
  
});




  document.forms[1].addEventListener("submit", (e) => {
    let isValid = true;

    const myName = document.getElementsByName("name")[0],
        phone = document.getElementsByName("phone")[0],
        email = document.getElementsByName("email")[0];
       
       

    // если условие сработает значение в форме будет считаться не правильным.
    if (myName.value.length === 0) {
        isValid = false;
        
    }
    if (phone.value.length === 0) {
        isValid = false;
       
    }
    if (email.value.length === 0) {
      isValid = false;
      
    }
   
    // В случае если форма заполнена не правильно - отображаем сообщение об ошибке 
    // и предотвращаем отправку запроса с помощью вызова preventDefault()
    
    if (!isValid) {
        e.preventDefault();
        alert("Всі поля вводу мають бути заповнені");
    }

   
    const patternPhone = /(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/,
    patternEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
    patternName = /[a-zA-Z0-9]+$/;

    let isValid2 = false;

    if (patternName.test(myName.value) && patternPhone.test(phone.value) && patternEmail.test(email.value)) {
      isValid2 = true;
    }


    if (!patternName.test(myName.value)) {
      e.preventDefault();
      
      alert(`Строка  ${myName.value}  НЕ відповідає шаблону<br />`);
    }


    
    if (!patternPhone.test(phone.value)) {
      e.preventDefault();
      
      alert(`Строка  ${phone.value}  НЕ відповідає шаблону<br />`);
      }
   

      if (!patternEmail.test(email.value)) {
      
      e.preventDefault();
      
      alert(`Строка  ${email.value}  НЕ відповідає шаблону<br />`);
    }

        if (isValid && isValid2 == true){
          e.preventDefault();
         document.location.href ="./thank-you.html";
  }

});






  
});
















/*
<form novalidate >
Name:
<input 
type="text" 
name="userName"
data-val="[a-zA-Z0-9]+"
data-val-msg="Введите значение"
data-val-msg-id="userNameMsg" />

<span id="userNameMsg"></span>
<br />

Email:
<input type="email" name="email"
    data-val="\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b"
    data-val-msg="Вы ввели не правильный email"
    data-val-msg-id="emailMsg"
    />
<span id="emailMsg"></span>
<br />

Zip:
<input type="number" name="zip"
    data-val="^\d{5}$"
    data-val-msg="Почтовый индекс введен не правильно"
    data-val-msg-id="zipMsg" />
<span id="zipMsg"></span>
<br />

<input type="submit" />
</form>*/


/*
 <form name="info" method="POST" action="./php/mail.php">
            <div class="grid">
                <label for="name">Ваше ім'я</label>
                <input placeholder="Iм'я" name="name" type="text">
                <label for="phone">Номер телефону</label>
                <input placeholder="+380505050500" name="phone" type="tel">
                <label for="email">Електронна пошта</label>
                <input placeholder="email@email.com" name="email" type="email">
                <input class='button' name="cancel" type="reset" value="Зкинути">
                <input class='button' name="btnSubmit" type="button" value="Підтвердити замовлення >>">
            </div>
        </form>



*/




/*
const pattern = /\d\d\d/;

let input = "hello 12 1world";
  if (pattern.test(input)) {
      document.write(`Строка ${input} соответствует шаблону<br />`);
  }
  else {
      document.write(`Строка  ${input}  НЕ соответствует шаблону<br />`);
  }

 input = "nkjsbgilvfjdgkvjre123";
  if (pattern.test(input)) {
      document.write(`Строка ${input} соответствует шаблону<br />`);
  }
  else {
      document.write(`Строка ${input} НЕ соответствует шаблону<br />`);
  }*/

/*

 function sumPrice(arr, pr){
    let x = 0;
    let r = 0;
    for( let i = 0; i < arr.length; i++ ){
      x += +arr[i]; 
      
    }
    r=x + pr;
    return r;
    
  }
 
*/




	

//https://temofeev.ru/info/articles/spisok-zadach-s-drag-amp-drop-na-chistom-javascript/




