/*Разработайте функцию-конструктор, которя будет создавать объект Human (человек). 
Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы массива по значинию свойства Age по возрастанию или по убыванию.

Разработайте функцию-конструктор, которая будет создавать объект Human (человек), добавьте на свое усмотрение свойства и методы в этот объект.
Подумайте, какие методы и свойства следует сделать уровня экземпляра, а какие уровня функции-конструктора.*/


function Human(name,age) { 
    
    this.firstName = name;
    this.age = age;


}


Human.prototype.sayHello = function () {
    document.write("Hello! My name is " + this.firstName + ".  "+"I am " + this.age + " years old. ");
}
 
Human.prototype.isAchild = function () {
    if(this.age<18) {
        document.write( "I am a child."+ "<br /> ")
    }
    else {
        document.write( "I am adult."+ "<br /> ")
    }
}



const hum1 = new Human("Alice", 32);
const hum = new Human("Mary", 50);
const hum2 = new Human("Jody", 15);



const humans = [hum1, hum, hum2];

function display (a) {
    for (let i=0; i<a.length; i++) {
        document.write(a[i].firstName + a[i].age+ "</br>");
    }
}

document.write("Before sorting"+ "</br>")+ display(humans)+ "</br>";

humans.sort((previous, next) => previous.age - next.age);
document.write("After sorting"+ "</br>")+ display(humans)+ "</br>";

humans.sort((previous, next) => previous.age - next.age).reverse();

document.write("After reversed sorting"+ "</br>")+ display(humans)+ "</br>";

function displayMethods (a) {
    for (let i=0; i<a.length; i++) {
        a[i].sayHello();
        a[i].isAchild();
    }
}

displayMethods (humans)




