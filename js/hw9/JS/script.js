/*Створіть програму секундомір.
            <br>
            * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"<br>
            * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
            * Виведення лічильників у форматі ЧЧ:ММ:СС<br>
            * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/



var div = document.querySelector(".stopwatch-display");




        const firstButtonHandler = () => {
            // установка значения для атрибута class (присвоение CSS класса для элемента)
            
           
            div.classList.remove("red");
            div.classList.remove("silver");
            div.classList.add("green");
        },

            secondButtonHandler = () => {

                
                div.classList.remove("green");
                div.classList.remove("silver");
                div.classList.add("red");
                
            },

            thirdButtonHandler = () => {
                
                div.classList.remove("red");
                div.classList.remove("green");
                div.classList.add("silver");
              
            };

            
            
     

            let counter = 0, intervalHandler;


            const ev = document.querySelector(".stopwatch-control");
            const display = document.querySelector(".stopwatch-display");
            
            
            const count = () => {
                let h = parseInt(counter/216000);
                let res = parseInt(counter-216000*h);
                let m = parseInt(res/3600);
                let s = parseInt((res - 3600*m)/60);
                

                display.lastElementChild.innerText = s; 
                display.firstElementChild.nextElementSibling.innerText = m;
                display.firstElementChild.innerText = h;
                counter++;
               
            }

            const reset= () => {
                display.lastElementChild.innerText = "00";
                display.firstElementChild.nextElementSibling.innerText = "00";
                display.firstElementChild.innerText = "00";
            }

            let startTimer = () => { intervalHandler = setInterval(count, 10);}

            ev.firstElementChild.onclick = () => {
                intervalHandler = setInterval(count, 10);
                firstButtonHandler(); 
            }
            

        
            ev.firstElementChild.nextElementSibling.onclick = () => {
                // останавливаем таймер, по которому вызывается функция count
                clearInterval(intervalHandler);
                secondButtonHandler();
            }

            
            ev.lastElementChild.onclick = () => { 
                thirdButtonHandler();
                clearInterval(intervalHandler);
                reset();
                
            } 


           //як доробити reset?
    

